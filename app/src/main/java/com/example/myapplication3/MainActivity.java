package com.example.myapplication3;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;

import android.content.pm.PackageManager;
import android.media.MediaPlayer;

import android.os.Build;
import android.os.Bundle;

import com.example.myapplication3.Models.SongInfos;
import com.example.myapplication3.adapter.SongInfoAdapter;
import com.example.myapplication3.data.SongData;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ListView listView;
    ImageView imageView;
    SeekBar seekBar;
    private int songPosition;
    private SongInfoAdapter songInfoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        seekBar = findViewById(R.id.seek_bar);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        imageView = findViewById(R.id.img_audio);
        SongData songData = new SongData();
        songData.test();
        songData.getPlayList(this);


    }


    private volatile boolean isSongPlaying;
    private int itemPosition;
    private int songDuration;
    private String musicFilePath;
    private void initListView(){
        final CardView cardView = findViewById(R.id.card_view);
        final TextView currentPosition = findViewById(R.id.currentPosition);
        final TextView duration = findViewById(R.id.song_duration);
       final Button playpause = findViewById(R.id.playpause);
        final Button next_button = findViewById(R.id.next_button);
        final  Button back_button = findViewById(R.id.back_button);
        final List<SongInfos> array = new SongData().getPlayList(this);
        listView = findViewById(R.id.list_view);
        TextAdapter textAdapter = new TextAdapter();
        textAdapter.data = array;
        listView.setAdapter(textAdapter);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int songProgress;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                songProgress = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                songPosition = songProgress;
                mp.seekTo(songProgress);
            }
        });
        playpause.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if(isSongPlaying){
                    mp.pause();
                    playpause.setText("play");
                }else{
                    mp.start();
                    playpause.setText("pause");
                }
                isSongPlaying = !isSongPlaying;
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Intent i = new Intent(getApplicationContext(), PlayMusicActivity.class);
//                i.putExtra("songTitle", array.get(position).getTitle());
//                startActivity(i);
                view.setSelected(true);
                cardView.setVisibility(View.VISIBLE);
                if(isSongPlaying){
                    mp.stop();
                    songPosition =0;
                }
                itemPosition = position;
                  musicFilePath = array.get(position).getFile_uri();
                songDuration = playMusicFile(musicFilePath)/1000;
                seekBar.setMax(songDuration);
                next_button.setOnClickListener(new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        mp.stop();
                        songPosition = 0;
                        seekBar.setProgress(songPosition);
                        musicFilePath = array.get(itemPosition++).getFile_uri();
                        songDuration = playMusicFile(musicFilePath)/1000;
                        seekBar.setMax(songDuration);
                        duration.setText(String.valueOf(songDuration/60)+ ":"+String.valueOf(songDuration%60));
                    }
                });
                back_button.setOnClickListener(new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                       if(itemPosition >= 0){
                           mp.stop();
                           songPosition = 0;
                           seekBar.setProgress(songPosition);
                           musicFilePath = array.get(itemPosition--).getFile_uri();
                           songDuration = playMusicFile(musicFilePath)/1000;
                           seekBar.setMax(songDuration);
                           duration.setText(String.valueOf(songDuration/60)+ ":"+String.valueOf(songDuration%60));
                       }
                    }
                });
                seekBar.setVisibility(View.VISIBLE);
                duration.setText(String.valueOf(songDuration/60)+ ":"+String.valueOf(songDuration%60));
                isSongPlaying = true;
                new Thread(){
                    public void run(){
                        songPosition =0;
                        while(songPosition < songDuration){
                            try {
                                Thread.sleep(1000);
                            }catch (InterruptedException e){
                                e.printStackTrace();
                            }
                            if(isSongPlaying){
                                songPosition++;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        seekBar.setProgress(songPosition);
                                        currentPosition.setText(String.valueOf(songPosition/60)+":"
                                                +String.valueOf(songPosition%60));
                                    }
                                });
                            }

                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mp.pause();
                                songPosition = 0;
                                mp.seekTo(songPosition);
                                currentPosition.setText("0");
                                playpause.setText("play");
                                isSongPlaying = false;
                                seekBar.setProgress(songPosition);
                            }
                        });
                    }

                }.start();
            }
        });
    }
    private MediaPlayer mp;
    private int playMusicFile(String path){
         mp = new MediaPlayer();

        try {
            mp.setDataSource(path);
            mp.prepare();
            mp.start();
        }catch (Exception e){
            e.printStackTrace();
        }
        return mp.getDuration();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private static final  String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE};
    private  static final int REQUEST_PERMISSION = 12345;
    private  static final  int PERMISSIONS_COUNT  = 1;
    @SuppressLint("NewApi")
    private boolean arePermissionsDenied(){
        for (int i = 0;i < PERMISSIONS_COUNT; i++){
            if(checkSelfPermission(PERMISSIONS[i]) != PackageManager.PERMISSION_GRANTED){
                return true;
            }
        }
        return false;
    }
    @SuppressLint("NewApi")
    public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults){
        super.onRequestPermissionsResult(requestCode,permissions,grantResults);
        if(arePermissionsDenied()){
            ((ActivityManager)(this.getSystemService(ACTIVITY_SERVICE))).clearApplicationUserData();
            recreate();
        }else{
            onResume();

        }
    }
    protected  void onResume(){
        super.onResume();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && arePermissionsDenied()){
          //  initRecyclerView();
            return;
        }
        initListView();
    }
    class TextAdapter extends BaseAdapter{
        private List<SongInfos> data = new ArrayList<>();

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
           if(convertView == null){
               convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.audio_list_item, parent, false);
               convertView.setTag(new ViewHolder((TextView)convertView.findViewById(R.id.song_name)));
           }
           ViewHolder holder = (ViewHolder) convertView.getTag();
           final String item = data.get(position).getTitle();
           holder.songName.setText((item));
           return  convertView;
        }
    }
    class ViewHolder{
        TextView songName;
        ViewHolder(TextView songName){
            this.songName  = songName;
        }
    }

}
