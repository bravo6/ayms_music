package com.example.myapplication3.data;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import com.example.myapplication3.Models.SongInfos;

import java.util.ArrayList;

public class SongData {
    private ArrayList<SongInfos> songList = new ArrayList<>();
    final Uri albumArtUri = Uri.parse("content://media/external/audio/albumart");
    public SongData(){}
    public void test(){
        Log.w("testons","testons");
    }
    public ArrayList<SongInfos> getPlayList(Context context) {

        {
            try {
                String[] proj = new String[]{
                        MediaStore.Audio.Media._ID,
                        MediaStore.Audio.Media.TITLE,
                        MediaStore.Audio.Media.ARTIST,
                        MediaStore.Audio.Media.DURATION,
                        MediaStore.Audio.Media.DATA,
                        MediaStore.Audio.Media.ALBUM,
                        MediaStore.Audio.Media.ALBUM_ID
                };
                Cursor audioCursor = context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, proj, null, null, null);


                if (audioCursor != null) {
                    Log.e("No problème","audio");
                    if (audioCursor.moveToFirst()) {
                        do {
                            int audioTitle = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE);
                            int audioartist = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ARTIST);
                            int audioduration = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION);
                            int audiodata = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
                            int audioalbum = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM);
                            int audioalbumid = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM_ID);
                            int song_id = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media._ID);
                            Log.e("tester", ""+audioTitle);
                            SongInfos info = new SongInfos();
                            info.setFile_uri(audioCursor.getString(audiodata));
                            info.setTitle(audioCursor.getString(audioTitle));
                            info.setDuration((audioCursor.getString(audioduration)));
                            info.setArtist(audioCursor.getString(audioartist));
                            info.setAlbum(audioCursor.getString(audioalbum));
                            info.setId(audioCursor.getLong(song_id));
                            Log.e("titre_chanson", info.getTitle());
                            info.setAlbum_art((ContentUris.withAppendedId(albumArtUri, audioCursor.getLong(audioalbumid))).toString());
                            songList.add(info);
                        } while (audioCursor.moveToNext());
                    }
                }else{
                    Log.w("not found","not found");
                }
                assert audioCursor != null;
                audioCursor.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return songList;
    }
}
