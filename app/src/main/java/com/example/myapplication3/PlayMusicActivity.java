package com.example.myapplication3;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class PlayMusicActivity extends AppCompatActivity {
    TextView textView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        Intent i = getIntent();
        setContentView(R.layout.play_audio);
        Log.e("data",i.getStringExtra("songTitle"));
        textView  = findViewById(R.id.song_title);
        textView.setText(i.getStringExtra("songTitle"));
    }
}
