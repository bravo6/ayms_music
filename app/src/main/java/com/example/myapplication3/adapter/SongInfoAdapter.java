package com.example.myapplication3.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication3.Models.SongInfos;
import com.example.myapplication3.R;

import java.util.ArrayList;

public class SongInfoAdapter extends RecyclerView.Adapter<SongInfoAdapter.SongHolder> {
    private Context context;
    private ArrayList<SongInfos> songInfos;

    public SongInfoAdapter(Context context, ArrayList<SongInfos> songInfos){
        this.context = context;
        this.songInfos = songInfos;
    }

    @NonNull
    @Override
    public SongHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.audio_list_item,parent,false);
        return  new SongHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SongHolder holder, int position) {
        SongInfos songIn = songInfos.get(position);
        holder.setSong(songIn);
    }

    @Override
    public int getItemCount() {
        return songInfos.size();
    }

    class SongHolder extends  RecyclerView.ViewHolder{
        private TextView songName;
        SongHolder(View itemView){
            super(itemView);
            songName = itemView.findViewById(R.id.song_name);
        }
        void setSong(SongInfos song){
            songName.setText(song.getTitle());
        }
    }

}
